#!/usr/bin/env python

import rospy
import numpy as np
from std_msgs.msg import Int16
from sensor_msgs.msg import Joy

list_size = 10

motor1_previous_state = 0
motor1_publisher = rospy.Publisher('motor1', Int16, queue_size=10) # Range from -4095 to 4095
motor1_list_size = list_size
motor1_list = np.zeros(motor1_list_size)

motor2_previous_state = 0
motor2_publisher = rospy.Publisher('motor2', Int16, queue_size=10) # Range from -4095 to 4095
motor2_list_size = list_size
motor2_list = np.zeros(motor2_list_size)

servo1_msg = Int16()
servo1_min_value = 100
servo1_max_value = 500
servo1_startup_value = 200
servo1_increment = 5
servo1_previous_state = 0
servo1_previous_value = servo1_startup_value
servo1_publisher = rospy.Publisher('servo1', Int16, queue_size=10) # Range from 0 to 4095

servo2_msg = Int16()
servo2_min_value = 100
servo2_max_value = 500
servo2_startup_value = 200
servo2_increment = 5
servo2_previous_state = 0
servo2_previous_value = servo2_startup_value
servo2_publisher = rospy.Publisher('servo2', Int16, queue_size=10) # Range from 0 to 4095

servo3_msg = Int16()
servo3_min_value = 100
servo3_max_value = 500
servo3_startup_value = 200
servo3_increment = 5
servo3_plus_previous_state = 0
servo3_less_previous_state = 0
servo3_previous_value = servo3_startup_value
servo3_publisher = rospy.Publisher('servo3', Int16, queue_size=10) # Range from 0 to 4095

laser_msg = Int16()
laser_startup_value = 0
laser_previous_state = 0
laser_publisher = rospy.Publisher('laser', Int16, queue_size=10) # Range from 0 to 1


def map_value(x, in_min, in_max, out_min, out_max):
    return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min


def set_init_value(msg, value, pub):
    msg = Int16()
    msg.data = value
    pub.publish(msg)


def ref_mode():
    set_init_value(servo1_msg, servo1_startup_value, servo1_publisher)
    set_init_value(servo2_msg, servo2_startup_value, servo2_publisher)
    set_init_value(servo3_msg, servo3_startup_value, servo3_publisher)
    set_init_value(laser_msg, laser_startup_value, laser_publisher)


def callback(data):

    # Motor 1
    motor1_tmp_data = data.axes[1]
    global motor1_previous_state
    if motor1_previous_state != motor1_tmp_data:
        global motor1_list
        motor1_list = np.delete(motor1_list, motor1_list_size-1)
        motor1_list = np.insert(motor1_list, 0, map_value(motor1_tmp_data, -1, 1, 1023, -1023))
        motor1_msg = Int16()
        motor1_msg.data = np.mean(motor1_list)
        motor1_publisher.publish(motor1_msg)
        motor1_previous_state = motor1_tmp_data


    # Motor 2
    motor2_tmp_data = data.axes[4]
    global motor2_previous_state
    if motor2_previous_state != motor2_tmp_data:
        global motor2_list
        motor2_list = np.delete(motor2_list, motor2_list_size-1)
        motor2_list = np.insert(motor2_list, 0, map_value(motor2_tmp_data, -1, 1, 1023, -1023))
        motor2_msg = Int16()
        motor2_msg.data = np.mean(motor2_list)
        motor2_publisher.publish(motor2_msg)
        motor2_previous_state = motor2_tmp_data


    # Servo 1
    servo1_tmp_data = data.axes[7]
    global servo1_previous_state
    global servo1_previous_value
    if servo1_tmp_data != servo1_previous_state:
        if servo1_tmp_data == 1:
            if (servo1_previous_value + servo1_increment) <= servo1_max_value:
                servo1_value = servo1_previous_value + servo1_increment # Compute value
                servo1_msg.data = servo1_value # Populate structure
                rospy.loginfo("Servo 1 New value : " + str(servo1_msg.data))
                servo1_previous_value = servo1_value # Previous servo value
                servo1_publisher.publish(servo1_msg) # Publish value
    if servo1_tmp_data != servo1_previous_state:
        if servo1_tmp_data == -1:
            if (servo1_previous_value - servo1_increment) >= servo1_min_value:
                servo1_value = servo1_previous_value - servo1_increment # Compute value
                servo1_msg.data = servo1_value # Populate structure
                rospy.loginfo("Servo 1 New value : " + str(servo1_msg.data))
                servo1_previous_value = servo1_value # Previous servo value
                servo1_publisher.publish(servo1_msg) # Publish value
    servo1_previous_state = servo1_tmp_data # Previous button value


    # Servo 2
    servo2_tmp_data = data.axes[6]
    global servo2_previous_state
    global servo2_previous_value
    if servo2_tmp_data != servo2_previous_state:
        if servo2_tmp_data == -1:
            if (servo2_previous_value + servo2_increment) <= servo2_max_value:
                servo2_value = servo2_previous_value + servo2_increment # Compute value
                servo2_msg.data = servo2_value # Populate structure
                rospy.loginfo("Servo 2 New value : " + str(servo2_msg.data))
                servo2_previous_value = servo2_value # Previous servo value
                servo2_publisher.publish(servo2_msg) # Publish value
    if servo2_tmp_data != servo2_previous_state:
        if servo2_tmp_data == 1:
            if (servo2_previous_value - servo2_increment) >= servo2_min_value:
                servo2_value = servo2_previous_value - servo2_increment # Compute value
                servo2_msg.data = servo2_value # Populate structure
                rospy.loginfo("Servo 2 New value : " + str(servo2_msg.data))
                servo2_previous_value = servo2_value # Previous servo value
                servo2_publisher.publish(servo2_msg) # Publish value
    servo2_previous_state = servo2_tmp_data # Previous button value


    # Servo 3
    global servo3_previous_value

    servo3_tmp_plus_data = data.buttons[5]
    global servo3_plus_previous_state
    if servo3_tmp_plus_data != servo3_plus_previous_state:
        if servo3_tmp_plus_data == 1:
            if (servo3_previous_value + servo3_increment) <= servo3_max_value:
                servo3_value = servo3_previous_value + servo3_increment # Compute value
                servo3_msg.data = servo3_value # Populate structure
                rospy.loginfo("Servo 3 New value : " + str(servo3_msg.data))
                servo3_previous_value = servo3_value # Previous servo value
                servo3_publisher.publish(servo3_msg) # Publish value
    servo3_plus_previous_state = servo3_tmp_plus_data # Previous button value

    servo3_tmp_less_data = data.buttons[4]
    global servo3_less_previous_state
    if servo3_tmp_less_data != servo3_less_previous_state:
        if servo3_tmp_less_data == 1:
            if (servo3_previous_value - servo3_increment) >= servo3_min_value:
                servo3_value = servo3_previous_value - servo3_increment # Compute value
                servo3_msg.data = servo3_value # Populate structure
                rospy.loginfo("Servo 3 New value : " + str(servo3_msg.data))
                servo3_previous_value = servo3_value # Previous servo value
                servo3_publisher.publish(servo3_msg) # Publish value
    servo3_less_previous_state = servo3_tmp_less_data # Previous button value


    # Laser
    laser_tmp_data = data.buttons[0]
    global laser_previous_state
    if laser_tmp_data != laser_previous_state:
        if laser_tmp_data == 1:
            laser_msg.data = laser_tmp_data # Populate structure
            rospy.loginfo("Laser : " + str(laser_msg.data))
            laser_publisher.publish(laser_msg) # Publish value
    laser_previous_state = laser_tmp_data # Previous button value


def listener():
    rospy.init_node('teleop_node', anonymous=True)
    rospy.Subscriber('joy', Joy, callback)
    ref_mode()
    rospy.spin()

if __name__ == '__main__':
    listener()
