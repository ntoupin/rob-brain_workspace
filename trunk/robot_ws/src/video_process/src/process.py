#!/usr/bin/env python

import cv2
import numpy as np
import rospy
from std_msgs.msg import Float32MultiArray


def image_process():
    img = cv2.imread('/home/Desktop/test.png', cv2.IMREAD_COLOR)
    img = cv2.medianBlur(img, 5)
    cimg = cv2.cvtColor(img, cv2.COLOR_GRAY2BGR)
    #Circle_Array = cv2.HoughCircles(img, cv2.HOUGH_GRADIENT, 1, 50, 200, 100)
    #if Circle_Array is not None:
    #    Circle_Array = np.round(Circle_Array[0, :]).astype("int")
    #    for (x, y, r) in Circle_Array:
    #        cv2.circle(cimg, (x, y), r, (0, 255, 0), 4)
    #        print('TEST')
    #        # print("Circle detected at " + str(x) + "," + str(y) + "," + str(r))
    if img is not None:
        cv2.imshow('Image', cimg)
        cv2.waitKey(0)
        cv2.destroyAllWindows()
    else:
        print('Image empty')


def callback(data):
    print('Test - ' + str(data.data[0]))
    # rospy.loginfo(rospy.get_caller_id() + 'I heard %s', data.data[0])
    image_process()


def listener():
    rospy.init_node('test_node', anonymous=True)
    rospy.Subscriber('hough_params', Float32MultiArray, callback)
    rospy.spin()


if __name__ == '__main__':
    listener()
